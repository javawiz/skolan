import java.util.Scanner;


public class Menu {
	
	private Skola skolan;
	
	public Menu() {
		skolan = new Skola();
	}
	
	public void runMenu() {
		boolean run = false;
		Scanner scan = new Scanner(System.in);
		
		do{
			
			int input = printChoises(scan);
			run = true;
			
			switch(input){
			case 1:
				String name = "erik";
				Person newPerson = new Student(name, 1988);
				skolan.addPerson(newPerson);
				
				break;
			case 3:
				skolan.printStudent();
				break;
			default:
				run = false;
			}
			
		}while(run);
	}
	
	private int printChoises(Scanner scanner) {
		System.out.println("Hej v�lkomen!");
		System.out.println("1. Add student");
		System.out.println("2. Add teacher");
		System.out.println("3. Print students");
		System.out.println("4. Print teachers");
		
		int input = scanner.nextInt();
		return input;
	}
	
	
}
