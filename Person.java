
public abstract class Person {
	
	private String name;
	private int birthyear;
	private Address address;
	
	public Person(String name, int birthyear) {
		this(name, birthyear, Address.getDefault());
	}
	
	public Person(String name, int birthyear, Address adress) {
		this.address = address;
		this.name = name;
		this.birthyear = birthyear;
	}
	
	@Override
	public String toString() {
		
		return name + ", year " + birthyear + ". address: " + address.get();
	}
	
}
