import java.util.ArrayList;


public class Skola {
	private ArrayList<Teacher> teachers;
	private ArrayList<Student> students;
	private ArrayList<Program> programs;
	
	public Skola() {
		teachers = new ArrayList<Teacher>();
		students = new ArrayList<Student>();
		programs = new ArrayList<Program>();
	}
	
	public void addPerson(Person person) {
		if (person instanceof Student) {
			students.add((Student)person);
		}else {
			teachers.add((Teacher)person);
		}
		
	}
	
	public void printStudent() {
		printPerson(students);
		
//		for(Person person: students) {
//			System.out.println(person.toString());
//		}
	}
	
	public void printTeachers() {
		printPerson(teachers);
		
//		for(Person person: teachers) {
//			System.out.println(person.toString());
//		}
	}
	
	private <T extends Person> void printPerson(ArrayList<T> people) {
		for(Person person : people) {
			System.out.println(person.toString());
			
		}
	}
	
	public void addStudentToProgram(Student student, Program program) {
		// TODO MAKE FIX
	}

}
