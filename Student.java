
public class Student extends Person {
	
	private float AvgGrades;
	
	
	public Student(String name, int birthyear) {
		super(name, birthyear);
		
		this.AvgGrades = 5;
		
	}
	
	@Override
	public String toString() {
		return "Student: " + super.toString() + "grades: " + AvgGrades;
	}

}
